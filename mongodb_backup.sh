#!/bin/bash

# For taking autobackup logs .
BACKUP_DIR="./backend backup"
DATE=$(date +%Y-%m-%d_%H-%M-%S)
DB_NAME="mydatabase"

# Vérifier si le répertoire de sauvegarde local existe, sinon le créer
mkdir -p "$BACKUP_DIR"

# MongoDB Lyes account used 
MONGO_URI="mongodb+srv://admin:meacMRwp91d6ftRt@atlascluster.um9vxp8.mongodb.net/$DB_NAME"

# Emplacement du binaire mongodump
MONGODUMP_PATH="./mongodump"

# Chemin vers l'outil AWS CLI 
# A rajouter dans le ansible | apt-get install -y awscli
AWS_CLI_PATH="/usr/bin/aws"

# Nom du bucket S3
S3_BUCKET="buckets3-frontend-0803"

# Mongodump tools installed on Intance. 
mongodump --uri="$MONGO_URI" --archive="$BACKUP_DIR/mongodb_backup_$DB_NAME_$DATE.gz" --gzip

# Copier la sauvegarde vers le répertoire local
cp "$BACKUP_DIR/mongodb_backup_${DB_NAME}_${DATE}.gz" "$BACKUP_DIR"

# Copier la sauvegarde vers le bucket S3
$AWS_CLI_PATH s3 cp "$BACKUP_DIR/mongodb_backup_${DB_NAME}_${DATE}.gz" "s3://$S3_BUCKET/"

echo "Backup for $DATE completed and uploaded to S3 bucket $S3_BUCKET."
